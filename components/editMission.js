import React, { Component } from 'react';
import {
	Text,
	View,
	Modal,
	TouchableHighlight,
	StyleSheet,
	Switch,
	TextInput,
	Picker,
	DatePickerIOS,
	Dimensions,
	ScrollView,
	Keyboard,
	TouchableWithoutFeedback
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import moment from 'moment'

import Header from './header';


export default class EditMission extends Component {
	constructor(props) {
		super(props)
		this.state = {
			assignee: this.props.mission.assignee ? this.props.mission.assignee : 'Unassigned',
			complete: this.props.mission.complete ? this.props.mission.complete : false,
		}
	}

	renderPicker(pickerToShow, pickerToHide) {
		this.setState({
			[pickerToShow]: true,
			[pickerToHide]: false,
		})
	}

	render() {
		const { stateUpdater, visible, mission, data, isNewMission } = this.props;

		// add 'Unassigned' option to client list if it doesn't already exist
		if (!data.clients.find(client => client.id === 0)) {
			data.clients.unshift({ email: "", id: 0, name: "Unassigned", user_id: 0 })
		}

		if (mission) {
			return (
				<Modal visible={visible} style={styleSheet.modal}>
					<View style={{ marginTop: 30 }}>

						{/* NAVBAR */}
						<View>
							<Header />
							<View style={styleSheet.navBar}>
								<TouchableHighlight
									onPress={() => {
										stateUpdater('selectedMission', null);
										stateUpdater('modalVisible', false);
										if (isNewMission) {
											data.missions.pop()
											stateUpdater('newMission', false);
										}
									}}>
									<View style={{ flexDirection: 'row', alignItems: 'center' }}>
										<FontAwesomeIcon icon={faChevronLeft} style={{ color: '#ffffff' }} />
										<Text style={styleSheet.navText}>{` Back to Table`}</Text>
									</View>
								</TouchableHighlight>
							</View>
						</View>

						{/* SAVE BUTTON */}
						<TouchableHighlight
							style={styleSheet.saveButton}
							onPress={() => {
								Object.assign(mission, this.state);
								stateUpdater('selectedMission', null);
								stateUpdater('modalVisible', false);
								stateUpdater('newMission', false);
							}}>
							<Text style={styleSheet.navText}> {`Save`} </Text>
						</TouchableHighlight>

						<ScrollView contentContainerStyle={{ marginHorizontal: '10%', alignItems: 'center', paddingBottom: 250 }}>

							{/* TITLE */}
							<Text style={[styleSheet.fieldTitles, { marginTop: 0 }]}>{`TITLE:`}</Text>
							<TextInput
								style={styleSheet.textInput}
								onChangeText={(text) => this.setState({ title: text })}
								value={this.state.title}
								clearButtonMode={'while-editing'}
								defaultValue={mission.title}
								keyboardType={'default'}
								multiline={true}
							/>

							{/* DATE */}
							<Text style={styleSheet.fieldTitles}>{`DATE:`}</Text>
							<TouchableWithoutFeedback onPress={() => { Keyboard.dismiss; this.renderPicker('showDatePicker', 'showUserPicker') }} accessible={false}>
								<Text style={styleSheet.textInput}>
									{moment(this.state.date).format('DD-MMM-YYYY')}
								</Text>
							</TouchableWithoutFeedback>
							{this.state.showDatePicker &&
								<DatePickerIOS
									style={styleSheet.Picker}
									initialDate={new Date(mission.date)}
									mode={'date'}
									onDateChange={(newDate) => this.setState({ date: newDate })}
								/>
							}

							{/* COMPLETED */}
							<View style={styleSheet.completedView}>
								<Text>{`COMPLETED:`}</Text>
								<Switch
									style={{ transform: [{ scaleX: .9 }, { scaleY: .9 }], alignSelf: 'flex-end' }}
									value={this.state.complete}
									onValueChange={(value) => this.setState({ complete: value })}
								/>
							</View>

							{/* ASSIGNEE */}
							<Text style={styleSheet.fieldTitles}>{`ASSIGNEE:`}</Text>
							<TouchableWithoutFeedback onPress={() => { Keyboard.dismiss; this.renderPicker('showUserPicker', 'showDatePicker') }} accessible={false}>
								<Text style={[styleSheet.textInput, { zIndex: 1, backgroundColor: 'white' }]}>
									{this.state.assignee}
								</Text>
							</TouchableWithoutFeedback>
							{this.state.showUserPicker &&
								<Picker
									style={[styleSheet.Picker, { marginTop: -50, zIndex: 0 }]}
									selectedValue={this.state.assignee}
									onValueChange={(itemValue) => this.setState({ assignee: itemValue })}
								>
									{data.clients.map((user) => {
										return <Picker.Item key={user.id} label={user.name} value={user.name} />
									})}
								</Picker>
							}
						</ScrollView>
					</View >
				</Modal >
			)
		}
		return null;
	}
}

const styleSheet = StyleSheet.create({
	navBar: {
		backgroundColor: '#43b2ff',
		alignSelf: 'stretch',
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between',
		borderRadius: 2,
		height: 39
	},
	navText: {
		color: '#ffffff',
		fontSize: 14,
		fontWeight: 'bold'
	},
	saveButton: {
		marginTop: 20,
		marginHorizontal: 10,
		width: 60,
		height: 40,
		alignSelf: 'flex-end',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#43b2ff',
		borderRadius: 50
	},
	fieldTitles: {
		marginVertical: 10,
		alignSelf: 'flex-start'
	},
	textInput: {
		width: '100%',
		padding: 10,
		borderColor: 'grey',
		borderWidth: 1,
		borderRadius: 10
	},
	Picker: {
		width: Dimensions.get('window').width,
	},
	completedView: {
		marginTop: 10,
		width: '100%',
		padding: 10,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	modal: {
		height: Dimensions.get('window').height,
	}
})