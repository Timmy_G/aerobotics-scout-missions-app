import React from 'react'
import EditMission from './editMission';

export default AddMission = (props) => {
	let { missions, data, stateUpdater, visible } = props
	const missionMeta = {
		complete: false,
		date: new Date,
		farm_id: 4006, // don't know if a user can schedule missions on multiple farms or not?
		id: 0,
		title: "",
		assignee: 'Unassigned'
	}

	if (missions && data) {
		
		if (missions.length > 0) {
			missionMeta.id = missions.sort((a, b) => b.id - a.id)[0].id + 1
		}

		missions.push(missionMeta);

		return (
			<EditMission
				isNewMission={true}
				visible={visible}
				stateUpdater={stateUpdater}
				data={data}
				mission={missions[missions.length - 1]}
			/>
		)
	} else {
		stateUpdater('data', { missions: [missionMeta], clients: [] });
		return null
	}
}
