import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, SafeAreaView, Dimensions, TouchableHighlight } from 'react-native';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons'

import getApiRequest from "../utils/apiRequests";
import EditMission from './editMission';
import TableRows from './tableRows';
import FilterData from './filter';
import AddMission from './newMission';


export default class MissionsTable extends Component {
	constructor(props) {
		super(props)
		this.state = {
			data: {},
			modalVisible: false,
			activeFilter: 'All',
		}
		this.stateUpdater = this.stateUpdater.bind(this)
	}
	static defaultProps = {
		tableHeaders: {
			title: 'TITLE',
			date: 'DATE',
			complete: 'COMPLETED',
			assignee: 'ASSIGNEE'
		},
		filters: ['All', 'Completed', 'Incomplete'],
	}

	componentDidMount() {
		this.fetchDataFromApi();
	}

	async fetchDataFromApi() {
		const apiRequest = await getApiRequest();

		const api_data = {
			clients: await apiRequest.get('clients/')
				.then((res) => { return res.data.results })
				.catch(err => console.log(err)),
			missions: await apiRequest.get('scoutmissions/')
				.then((res) => { return res.data.results })
				.catch(err => console.log(err)),
		}

		if (await api_data) {
			this.setState({ data: api_data })
		};
	}


	stateUpdater(key, value) {
		this.setState({ [key]: value })
	}



	render() {
		const { tableHeaders, filters } = this.props;
		let { filteredMissions, data, activeFilter } = this.state
		const { missions } = data;

		if (!filteredMissions) {
			filteredMissions = missions;
		}

		return (
			<SafeAreaView style={styleSheet.ScrollView}>
				<ScrollView stickyHeaderIndices={[0]} contentContainerStyle={{ paddingBottom: 200 }}>
					<View>
						<View style={styleSheet.filters}>
							<Text>{`Filters: `}</Text>
							{filters.map((filter, index) => {
								return (
									<FilterData key={index} filterKey={filter} missions={missions} stateUpdater={this.stateUpdater} activeFilter={activeFilter} />
								)
							})}
						</View>
						<View style={styleSheet.tableHeaderContainer}>
							<TableRows type='thead' values={tableHeaders} styleProps={{ color: '#ffffff', fontWeight: 'bold' }} />
						</View>
					</View>
					{filteredMissions && filteredMissions.length > 0 &&
						<View>
							<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
								{
									filteredMissions.sort((a, b) => {
										if (moment(b.date).isAfter(a.date)) {
											return -1
										}
									}).map((mission) => {
										return (
											<TableRows key={mission.id} type='tbody' values={mission} styleProps={{ color: '#202020', fontWeight: 'normal' }} stateUpdater={this.stateUpdater} />
										)
									})
								}
							</View>
							{this.state.selectedMission &&
								<EditMission
									visible={this.state.modalVisible}
									stateUpdater={this.stateUpdater}
									data={this.state.data}
									mission={filteredMissions.find(mission => mission.id === this.state.selectedMission)}
								/>
							}
						</View>
					}
					<View>
						{this.state.newMission &&
							<AddMission missions={missions} data={this.state.data} stateUpdater={this.stateUpdater} visible={this.state.newMission} />
						}
					</View>
					<TouchableHighlight
						style={{ alignSelf: 'center', marginVertical: 15 }}
						onPress={() => { this.setState({ 'newMission': true }) }}>
						<FontAwesomeIcon icon={faPlusCircle} style={{ color: '#43b2ff' }} size={50} />
					</TouchableHighlight>
				</ScrollView>
			</SafeAreaView>
		)
	};
}

const styleSheet = StyleSheet.create({
	filters: {
		backgroundColor: '#fbfbfb',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'baseline'
	},
	tableHeaderContainer: {
		backgroundColor: '#43b2ff',
		alignSelf: 'stretch',
		borderRadius: 2
	},
	ScrollView: {
		height: Dimensions.get('window').height,
	}
})