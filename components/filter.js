import React from 'react'
import { TouchableHighlight, Text, StyleSheet } from 'react-native';

export default FilterData = (props) => {
	const { filterKey, missions, stateUpdater, activeFilter } = props

	filterByKey = {
		All: () => { return missions },
		Completed: () => { return missions.filter(mission => mission.complete) },
		Incomplete: () => { return missions.filter(mission => !mission.complete) },
	}

	return (
		<TouchableHighlight
			style={activeFilter === filterKey ? styleSheet.activeFilter : styleSheet.inactiveFilters}
			onPress={() => {
				stateUpdater('activeFilter', filterKey)
				return stateUpdater('filteredMissions', filterByKey[filterKey]());
			}}>
			<Text style={activeFilter === filterKey ? styleSheet.activeText : { textAlign: 'center' }}>{`${filterKey}`}</Text>
		</TouchableHighlight>
	)
}


const styleSheet = StyleSheet.create({
	activeFilter: {
		backgroundColor: '#43b2ff',
		borderRadius: 50,
		paddingHorizontal: 15,
		paddingVertical: 10,
		marginBottom: 15,
	},
	inactiveFilters: {
		backgroundColor: '#f0f0f0',
		borderRadius: 50,
		paddingHorizontal: 15,
		paddingVertical: 10,
		marginBottom: 15,
	},
	activeText: {
		color: '#ffffff',
		textAlign: 'center'
	}
})