import React, { Component, Fragment } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'


export default class TableRows extends Component {
	constructor(props) {
		super(props)
	}

	renderRowColumns() {
		let { values, styleProps, type } = this.props;

		const date = moment(values.date, "YYYY-MM-DD").isValid() ? moment(values.date).format('DD MMM') : values.date;

		return (
			<Fragment>
				<View style={{ flex: 1, flexShrink: 1, alignSelf: 'flex-start' }}>
					<Text style={[styleProps, { alignSelf: 'flex-start' }]}>{`${date}`}</Text>
				</View>
				<View style={{ flex: 1, flexGrow: 1, alignSelf: 'stretch' }}>
					<Text style={styleProps}>{`${values.title}`}</Text>
				</View>
				<View style={{ flex: 1, flexShrink: 1, alignSelf: 'stretch' }}>
					{type === 'thead' &&
						<Text style={styleProps}>{`${values.complete}`}</Text>
					}
					{values.complete && typeof values.complete === 'boolean' &&
						<FontAwesomeIcon icon={faCheck} style={{ alignSelf: 'center' }} />
					}
				</View>
				<View style={{ flex: 1, alignSelf: 'baseline' }}>
					<Text style={[styleProps, { alignSelf: 'flex-end', textAlign: 'right' }]}>{`${values.assignee ? values.assignee : 'Unassigned'}`}</Text>
				</View>
			</Fragment>
		)

	}

	render() {
		const { type, stateUpdater, values } = this.props;

		if (type === 'thead') {
			return (
				<View style={styleSheet.headerRowView}>
					{this.renderRowColumns()}
				</View>
			)
		}
		else {
			return (
				<TouchableOpacity
					style={styleSheet.TouchableOpacity}
					onPress={() => {
						stateUpdater('modalVisible', true);
						stateUpdater('selectedMission', values.id);
					}}>
					{this.renderRowColumns()}
				</TouchableOpacity>
			);
		}
	}
}

const styleSheet = StyleSheet.create({
	TouchableOpacity: {
		flex: 1,
		alignSelf: 'stretch',
		alignItems: 'baseline',
		flexDirection: 'row',
		margin: 1,
		backgroundColor: '#ffffff',
		borderRadius: 10,
		paddingVertical: 20,
		paddingHorizontal: 5
	},
	headerRowView: {
		flex: 1,
		alignSelf: 'stretch',
		flexDirection: 'row',
		paddingBottom: 2,
		margin: 10
	},
})
