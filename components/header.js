import React from 'react';
import { View, Dimensions, Image } from 'react-native';
const logo = require('../assets/aerobotics_logo_new.png')


export default function Header() {
	return (
		<View style={{ display: 'flex', flexDirection: 'row' }}>
			<Image
				source={logo}
				style={{
					flex: 1,
					height: 50,
					width: 50,
					maxWidth: Dimensions.get('window').width,
					resizeMode: 'center',
					marginBottom: 30,
					padding: 40
				}}
			/>
		</View>
	)
}
