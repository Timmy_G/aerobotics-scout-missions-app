import axios from 'axios';

export default async function getApiRequest() {
	const apiReq = axios.create({
		baseURL: "https://sherlock.aerobotics.io/developers/",
		headers: {
			Authorization: '1536660107LWZ2JGK17J72HR4O5NU53FBBSLSMRB',
		}
	});
	return apiReq;
}