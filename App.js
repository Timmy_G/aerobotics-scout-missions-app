/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { SafeAreaView, View } from 'react-native';

import MissionsTable from './components/missionsTable'
import Header from './components/header';

const App = () => {
	return (
		<SafeAreaView>
			<View style={{ backgroundColor: '#fafafa' }}>
				<View style={{ marginTop: 30 }}>
					<Header />
				</View>
				<MissionsTable />
			</View>
		</SafeAreaView>
	);
};

export default App;
